﻿using UnityEngine;
using System.Collections;

public class ZombieScript : MonoBehaviour {

    public GameObject player;
    public Animator anim;
    public float speed;
    public int health;
    public int damage;
    public float attack_anim_time = 1.433f;
    public float death_anim_time = 1.433f;
    public bool able_to_attack = true;
    public float attack_countdown = 0;
    public bool has_dealt_damage = false;
    
    //Initialize the variables we'll be using.
	void Start () {
        player = GameObject.FindGameObjectWithTag("player");
        anim = GetComponentInChildren<Animator>();
	}
	
    //I like to separate out the update to keep it cleaner.
	void Update () {
        Movement();
        Attack_Tracker();
    }

    //Because the animations on the model are locked as .FBX, I can't add keyframes to them.  I would normally do this with adding a keyframe that links to an event in here
    //A lot of this ends up being a workaround.  This is essentially tracking how long until the animation to attack can play again.  It stops the zombie once it is attacking,
    //and prevents extra damage being done or the animation playing again due to a bool or a trigger being used.
    public void Attack_Tracker()
    {
        if (attack_countdown > 0)
        {
            attack_countdown -= Time.deltaTime;
        }
        if (attack_countdown <= 0)
        {
            attack_countdown = 0;
            able_to_attack = true;
            has_dealt_damage = false;
        }
    }

    //If we're able to attack again, the zombie can move!  Just rotates towards the player and moves towards it
    public void Movement()
    {
        if (able_to_attack)
        {
            transform.LookAt(new Vector3(player.transform.position.x, transform.position.y, player.transform.position.z));
            transform.Translate(Vector3.forward * Time.deltaTime * speed); 
        }
    }

    //Check to see if we're colliding with the player, then hurt them!
    public void OnTriggerStay(Collider other)
    {
        if (other.tag == "player" )
        {
            //I normally just use transitions, but because the FBX file has set animations where I can't add event frames,
            //This is the easiest way to get the behavior I want (Zombie stops moving when it's attacking you, doesn't loop attack erroneously because of trigger)
            anim.Play("attack");
            able_to_attack = false;
            attack_countdown = attack_anim_time;   
            if (!has_dealt_damage)
            {
                StartCoroutine(Hurt_Player());
                has_dealt_damage = true;
            }
        }      
    }

    //I split this out into a coroutine so that I can do the damage during the animation.  Again, this would be accomplished easily with keyframes.
    public IEnumerator Hurt_Player()
    {
        yield return new WaitForSeconds(1);
        player.GetComponent<PlayerStatsScript>().Health_Change(-damage);
    }

    //When the zombie gets hurt.  I would write an interface for this if I was building this project to scale.
    public void Health_Change(int change)
    {
        health += change;
        if (health <= 0)
        {
            anim.Play("back_fall");
            able_to_attack = false;
            attack_countdown = death_anim_time;
            Destroy(gameObject, death_anim_time);
        }
    }

    //Increase the points when the zombies are dead.  This also means that when they're cleared at the end of a round, they increase your score.
    public void OnDestroy()
    {
        player.GetComponent<PlayerStatsScript>().current_score++;
    }
}
