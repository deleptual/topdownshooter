﻿using UnityEngine;
using System.Collections;

//The projectile that the skeleton uses to kill zombies.
public class Fireball : MonoBehaviour {

    public float speed = 15;
    public Vector3 direction;
    public float current_lifetime = 6;
    public int damage = 1;
    
    //In the update we want to move it and check it's lifetime.
	void Update () {
        current_lifetime -= Time.deltaTime;
        if (current_lifetime <= 0)
        {
            Destroy(gameObject);
        }
        transform.Translate(direction * speed * Time.deltaTime);
	}

    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "enemy")
        {
            //In the future I would build this out as an interface so it worked for each enemy instead of just hardcoding in with zombie scripts.
            other.gameObject.GetComponent<ZombieScript>().Health_Change(-damage);
            Destroy(gameObject);
        }
    }
}
