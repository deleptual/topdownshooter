﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

//Just manages the title screen, allowing us to load the highscore and move to the actual game.
public class TitleScript : MonoBehaviour {

    public Text txt_highscore;

	void Start () {
	    if (PlayerPrefs.HasKey("HighScore"))
        {
            txt_highscore.text = PlayerPrefs.GetInt("HighScore").ToString();
        }
	}

	void Update () {
	    if (Input.GetKey(KeyCode.Return))
        {
            SceneManager.LoadScene("main");
        }
	}
}