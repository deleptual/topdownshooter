﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

//A lot of the data is stored inside here.
public class PlayerStatsScript : MonoBehaviour {

    public int current_score = 0;
    public int high_score;
    public float speed = 1;
    public int max_ammo = 12;
    public int current_ammo = 12;
    public bool can_fire = true;
    public int lives = 3;
    public int health = 2;
    public float reload_time = 2;
    public float current_reload_time = 0;
    public GameObject fireball;
    public Text txtlives;
    public Text txtscore;
    public Text txtbullets;
    public Text txthealth;
    public float x_min = -12;
    public float x_max = 6;
    public float z_min = -2;
    public float z_max = 11;
    public float offset = 0.05f;
    public GameManager gm;

    //When the zombies kill, set the game up again to be played.
    public void Initialize_Game()
    {
        speed = 0.1f;
        max_ammo = 12;
        current_ammo = 12;
        can_fire = true;
        current_reload_time = 0;
        health = 2;
        //Destroys all the enemies on the board between rounds.
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("enemy");
        for (int i = 0; i < enemies.Length; i++)
        {
            Destroy(enemies[i]);
        }
    }

    //Again, I like to split out my logic so it's easier for me (and others) to follow.
    void Update()
    {
        Movement();
        Follow_Cursor();
        Attack();
        Reload();
        Update_UI();
    }

    //Keeps the UI updated every frame.  If there was performance issues, this could be stored in a coroutine instead of the update.
    public void Update_UI()
    {
        txtlives.text = lives.ToString();
        txtbullets.text = current_ammo.ToString();
        txtscore.text = current_score.ToString();
        txthealth.text = health.ToString();
    }

    //The player spins around to follow the cursor.  Basically draws a line from the camera to the mouse that keeps going until it intersects the plane
    //that the player is on.  Once we have that ray, we can find out how far along the ray it is.  And then we convert that point to a vector 
    //And look at it.
    public void Follow_Cursor()
    {
        Plane playerPlane = new Plane(Vector3.up, transform.position);
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        float distance = 0;
        if (playerPlane.Raycast (ray, out distance))
        {
            Vector3 targetPoint = ray.GetPoint(distance);
            transform.LookAt(targetPoint);
        }
    }

    //When the player needs to reload.  You have to trigger this with the right mouse button
    public void Reload()
    {
        if (current_reload_time > 0)
        {
            current_reload_time -= Time.deltaTime;
            if (current_reload_time <= 0)
            {
                current_reload_time = 0;
                can_fire = true;
                current_ammo = max_ammo;
            }
        }
        if (Input.GetMouseButtonDown(1))
        {
            max_ammo = 12;
            can_fire = false;
            current_reload_time = 2;
        }
    }

    //If we have ammo, we can fire.  There is no cooldown between shots.
    public void Attack()
    {
        if (can_fire)
        {
            if (Input.GetMouseButtonDown(0))
            {
                GameObject projectile = Instantiate(fireball, transform.position + new Vector3(0,1,0), transform.localRotation) as GameObject;
                projectile.GetComponent<Fireball>().direction = Vector3.forward;
                current_ammo--;
                if (current_ammo <= 0)
                {
                    can_fire = false;
                }
            }
        }
    }

    //Just translates the character around, and keeps him in the world bounds.
    public void Movement()
    {
        if (Input.GetKey(KeyCode.A) && transform.position.x > x_min + offset)
        {
            transform.Translate(new Vector3(-1 ,0 ,0) * speed, Space.World);
        }
        if (Input.GetKey(KeyCode.W) && transform.position.z < z_max - offset)
        {
            transform.Translate(new Vector3(0, 0, 1) * speed, Space.World);
        }
        if (Input.GetKey(KeyCode.S) && transform.position.z > z_min + offset)
        {
            transform.Translate(new Vector3(0, 0, -1) * speed, Space.World);
        }
        if (Input.GetKey(KeyCode.D) && transform.position.x < x_max - offset)
        {
            transform.Translate(new Vector3(1, 0, 0) * speed, Space.World);
        }
    }

    //When the player takes damage we call this function.  It also ends the game.
    public void Health_Change(int change)
    {
        health += change;
        if (health <= 0)
        {
            lives--;
            if (lives <= 0)
            {
                gm.End_Game(current_score);
            }
            else
            {
                Initialize_Game();
            }
        }
    }
}
