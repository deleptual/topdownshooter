﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    public bool paused = false;
    public bool game_ended = false;
    public GameObject pause_panel;
    public GameObject end_panel;
    public Text txt_highscore;
    public Text txt_finalscore;
    public List<GameObject> graves = new List<GameObject>();
    public float game_time = 0;
    public float x_min = -12;
    public float x_max = 6;
    public float z_min = -2;
    public float z_max = 11;
    public float offset = 0.05f;
    public GameObject weak_zombie;
    public GameObject strong_zombie;
    public GameObject tombstone;
    public float difficulty = 4;

    void Start()
    {
        StartCoroutine(Spawn());
    }

    //Pausing the game is checked for in here, as well as increasing the difficulty over time.  If the game is over, you can press
    //return to start again.
    void Update () {
	    if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (paused)
            {
                Unpause();
            }
            else
            {
                Pause();
            }
        }
        game_time += Time.deltaTime;
        Adjust_Difficulty();
        if (game_ended)
        {
            if (Input.GetKey(KeyCode.Return))
            {
                Time.timeScale = 1;
                SceneManager.LoadScene("title");
            }
        }
	}

    //Just a simple math equation to ramp up the difficulty pretty quickly.
    public void Adjust_Difficulty()
    {
        if (game_time > 10)
        {
            difficulty = Mathf.Sqrt(difficulty);
            game_time = 0;
        }
    }

    //This coroutine calls itself at the end again, each time getting quicker and quicker as the variable for difficulty gets smaller.
    //Equal chance for a weak zombie or a strong zombie, and all 4 map edges.
    public IEnumerator Spawn() 
    {
        yield return new WaitForSeconds(difficulty);
        //Choose which edge to start at, and which zombie to spawn.  Then we spawn randomly along that edge.
        int edge = Random.Range(0, 4);
        int strong = Random.Range(0, 2);
        switch (edge)
        {
            case 0:
                if (strong == 0)
                {
                    Instantiate(strong_zombie, new Vector3(x_min, 0, Random.Range(z_min, z_max)), Quaternion.identity);
                }
                else
                {
                    Instantiate(weak_zombie, new Vector3(x_min, 0, Random.Range(z_min, z_max)), Quaternion.identity);
                }
                break;
            case 1:
                if (strong == 0)
                {
                    Instantiate(strong_zombie, new Vector3(Random.Range(x_min, x_max), 0, z_max), Quaternion.identity);
                }
                else
                {
                    Instantiate(weak_zombie, new Vector3(Random.Range(x_min, x_max), 0, z_max), Quaternion.identity);
                }
                break;
            case 2:
                if (strong == 0)
                {
                    Instantiate(strong_zombie, new Vector3(x_max, 0, Random.Range(z_min, z_max)), Quaternion.identity);
                }
                else
                {
                    Instantiate(weak_zombie, new Vector3(x_max, 0, Random.Range(z_min, z_max)), Quaternion.identity);
                }
                break;
            case 3:
                if (strong == 0)
                {
                    Instantiate(strong_zombie, new Vector3(Random.Range(x_min, x_max), 0, z_min), Quaternion.identity);
                }
                else
                {
                    Instantiate(weak_zombie, new Vector3(Random.Range(x_min, x_max), 0, z_min), Quaternion.identity);
                }
                break;
            default:
                break;               
        }
        StartCoroutine(Spawn());
    }

    public void Quit_Game()
    {
        Application.Quit();
    }

    public void Pause()
    {
        pause_panel.SetActive(true);
        Time.timeScale = 0;
        paused = true;
    }

    public void Unpause()
    {
        paused = false;
        Time.timeScale = 1;
        pause_panel.SetActive(false);
    }

    //Pulls the score in, and checks to see if you have a new high score.
    //Freezes the game, displays the end panel, and lets you restart if you'd like to.
    public void End_Game(int current_score)
    {
        game_ended = true;
        end_panel.SetActive(true);
        Time.timeScale = 0;
        int high_score;
        if (PlayerPrefs.HasKey("HighScore"))
        {
            high_score = PlayerPrefs.GetInt("HighScore");
            if (current_score > high_score)
            {
                PlayerPrefs.SetInt("HighScore", current_score);
                high_score = current_score;
            }
        }
        else
        {
            PlayerPrefs.SetInt("HighScore", current_score);
            high_score = current_score;
        }
        txt_finalscore.text = current_score.ToString();
        txt_highscore.text = high_score.ToString();
    }
}
